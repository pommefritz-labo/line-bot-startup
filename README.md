# Line Bot
## Setup 準備
1. Line アクセストークン
2. docker setup
3. DB setup
4. ngrok setup

## Line アクセストークン
1. Lineの開発者管理画面からアクセストークンを取得
2. .env.exampleをコピー
```
cp .env.example .env
```
3. .envのLINE_BOT_APIにアクセストークンを記入

## Setup with Docker
## ポート番号
外部からDocker DBに接続するポートを13306、  
外部からDocker APIに接続するポートを13000に指定。

### run server
```
$ docker-compose up
```
`localhost:13000` でアクセス可能
### stop server
```
$ docker-compose stop
```
### Docker api container にアクセス
```
$ docker-compose exec api bash
```
## DB Setup 
```
# migrate
$ docker-compose run api python manage.py migrate
* line table だけmigrateする場合 *
# docker-compose run api python manage.py migrate line
```
## (開発用)ローカルサーバーをネットワークに公開
### ngrok を使用
#### Installation
1. ngrokをダウンロード
https://ngrok.com/download
2. ダウンロードしたファイルを解凍
3. 解凍されたngrokが存在するディレクトリで以下のコマンドを入力
```
./ngrok http 13000
```
4. 生成されたurlを
  - settings.pyのALLOWED_HOSTSに追加する
  - Lineの開発コンソール上にあるWebhookに追加する
##### 補足
ngrokはhomebrewでもインストール可能とのこと  
ngrokを起動させるとhttp://127.0.0.1:4040 から、  
ngrokのwebコンソールにアクセス出来る  

## コマンド集
```
# make migration
$ docker-compose run api python manage.py makemigrations
# test
$ docker-compose run api python manage.py test
```
## Line 公式アカウント
### Line Developers
https://developers.line.biz/ja/services/
### Line Official Account Manager
https://manager.line.biz/
### Flex Message Simulator
https://developers.line.biz/console/fx/
### Line Message API
https://developers.line.biz/ja/services/messaging-api/
