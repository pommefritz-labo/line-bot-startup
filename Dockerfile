FROM python:3.7
ENV PYTHONNUNBUFFERED 1

RUN ["apt-get", "update"]
RUN ["apt-get", "install", "-y", "vim", "binutils", "libproj-dev", "gdal-bin"]

RUN mkdir /app
WORKDIR /app
COPY requirements.txt /app/
RUN pip3 install -r requirements.txt
COPY . /app/

EXPOSE 80
CMD [ "python3","manage.py","runserver","0.0.0.0:80" ]
