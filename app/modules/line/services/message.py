# -*- coding: utf-8 -*-
from ..models import LineMessage
from ..forms import LineMessageForm


def get_or_create_line_message(**kwargs):
    """
    :param kwargs dict:
    :rtype: LineMessage, created
    """
    return LineMessage.get_or_create(**kwargs)


def create_line_message(**kwargs):
    """
    :param kwargs dict:
    :rtype: LineMessage
    """
    return LineMessage.create(**kwargs)


def filter_by_push(push):
    """
    :param push LinePush
    :rtype: LineMessage
    """
    return LineMessage.filter_by_push(push=push)


def get_line_message_form(**kwargs):
    """
    :param kwargs:
    :rtype: LineMessageForm
    """
    return LineMessageForm(**kwargs)


def post_line_message_form(query_dict):
    """
    :param QueryDict query_dict:
    :rtype: LineMessageForm
    """
    return LineMessageForm(query_dict)
