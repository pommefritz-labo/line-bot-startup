# -*- coding: utf-8 -*-
from ..models import LinePush


def get_by_id(_id: int):
    """
    :param _id int:
    :rtype: LinePush or None
    """
    return LinePush.get_by_id(_id=_id)


def get_or_create_line_push(**kwargs):
    """
    :param kwargs dict:
    :rtype: (LinePush, created)
    """
    return LinePush.get_or_create(**kwargs)


def filter_by_user_id(user_id: int):
    """
    :param user_id int:
    :rtype: Queryset
    """
    return LinePush.filter_by_user_id(user_id)


def switch_is_blocked_by_user_id(user_id: int, switch: bool):
    """
    :param switch bool
    :rtype:
    """
    return filter_by_user_id(user_id=user_id).update(is_blocked=switch)
