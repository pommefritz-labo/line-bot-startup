# -*- coding: utf-8 -*-
import enum


class MessageAttribute(enum.IntEnum):
    FromPush = 1
    Bot = 2
    Manual = 3

    @classmethod
    def get_choices(cls):
        choice_list = [(obj.value, cls.get_jp(obj.value)) for obj in cls]
        return tuple(choice_list)

    @classmethod
    def get_jp(cls, value):
        opt = {
            cls.FromPush.value: 'ユーザー',
            cls.Bot.value: 'ボット',
            cls.Manual.value: '手動返信',
        }
        return opt[value]
