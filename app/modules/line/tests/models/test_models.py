# -*- coding: utf-8 -*-
from django.test import TestCase

from ...models import LinePush, LineMessage
from ..factories import LinePushFactory, LineMessageFactory


class LinePushTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.model = LinePush
        cls.line_push = LinePushFactory()


class LineMessageTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.model = LineMessage
        cls.line_message = LineMessageFactory()
