# -*- coding: utf-8 -*-
from django.test import TestCase

from ...services import services
from ..factories import LinePushFactory, LineMessageFactory


class LineServiceTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.sv = services
        cls.line_push = LinePushFactory()
        cls.line_message = LineMessageFactory()
