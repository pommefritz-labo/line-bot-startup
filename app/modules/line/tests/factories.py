# -*- coding:utf-8 -*-
import factory.fuzzy

from ..models import LinePush, LineMessage


class LinePushFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = LinePush

    user_id = factory.Faker('name')
    display_name = factory.Faker('name')


class LineMessageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = LineMessage

    push = factory.SubFactory(LinePushFactory)
    text = factory.Faker('name')
    msg_attr = factory.Faker('random_int', min=1, max=3)
