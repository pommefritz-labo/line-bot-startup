from django.db import models


class LinePush(models.Model):
    """Lineでのプッシュ先"""
    class Meta:
        db_table = 'line_push'
        verbose_name = 'ラインボット登録ユーザー'
        verbose_name_plural = '全てのラインボット登録ユーザー'

    user_id = models.CharField('ユーザーID', max_length=100, unique=True)
    display_name = models.CharField('表示名', max_length=255, blank=True)
    is_blocked = models.BooleanField('ブロックされた場合', default=False)

    @classmethod
    def get_by_id(cls, _id):
        try:
            return cls.objects.get(pk=_id)
        except cls.DoesNotExist:
            return None

    @classmethod
    def get_or_create(cls, **kwargs):
        return cls.objects.get_or_create(**kwargs)

    @classmethod
    def filter_by_user_id(cls, user_id):
        return cls.objects.filter(user_id=user_id)

    def __str__(self):
        return self.display_name
