from django.db import models
from django.utils import timezone
from .push import LinePush
from ..constants import MessageAttribute as msg


class LineMessage(models.Model):
    """Lineの各メッセージを表現する"""
    push = models.ForeignKey(
        LinePush,
        verbose_name='プッシュ先',
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    text = models.TextField('テキスト')
    msg_attr = models.IntegerField('発言者属性', default=msg.FromPush, choices=msg.get_choices())
    created_at = models.DateTimeField('作成日', default=timezone.now)

    @classmethod
    def get_or_create(cls, **kwargs):
        return cls.objects.get_or_create(**kwargs)

    @classmethod
    def create(cls, **kwargs):
        return cls.objects.create(**kwargs)

    @classmethod
    def filter_by_push(cls, push):
        return cls.objects.filter(push=push)

    def __str__(self):
        return f'{self.push} - {self.text[:10]} - {self.msg_attr}'
