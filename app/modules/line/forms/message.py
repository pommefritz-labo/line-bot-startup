# -*- coding: utf-8 -*-
from django import forms
from ..models import LineMessage


class LineMessageForm(forms.ModelForm):
    class Meta:
        model = LineMessage
        fields = (
            'text',
            'push',
            'msg_attr',

        )
        widgets = {
            'text': forms.Textarea(attrs={'class': 'form-control'}),
            'push': forms.HiddenInput(attrs={'class': 'form-control'}),
            'msg_attr': forms.HiddenInput(attrs={'class': 'form-check-input'}),
        }
        labels = {
        }

    def __init__(self, *args, **kwargs):
        super(LineMessageForm, self).__init__(*args, **kwargs)
