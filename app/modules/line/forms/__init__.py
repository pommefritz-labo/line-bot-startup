# -*- coding: utf-8 -*-
from .message import LineMessageForm

__all__ = [
    # message.py
    'LineMessageForm',
]
