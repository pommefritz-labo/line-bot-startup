from django.urls import path
from django.conf.urls import include
from django.contrib import admin

app_name = 'app'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('app.web.callback.urls', namespace='callback')),
    path('', include('app.web.sys.urls', namespace='sys')),
]
