# -*- coding: utf-8 -*-
from django.urls import path

from . import views


urlpatterns = [
    path('callback/', views.CallbackView.as_view(), name='callback'),
]
