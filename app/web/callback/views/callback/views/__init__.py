# -*- coding: utf-8 -*-
from .main import CallbackView

__all__ = [
    # main.py
    'CallbackView',
]
