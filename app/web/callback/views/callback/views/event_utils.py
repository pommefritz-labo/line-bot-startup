# -*- coding: utf-8 -*-
import os

from app.modules.line import services as line_sv
from app.modules.line.constants import MessageAttribute
from .bot_script import bot_script_dict

import linebot
from linebot.models import (
    TextSendMessage,
)

line_bot_api = linebot.LineBotApi(os.environ['LINE_BOT_API'])


def follow(line_user_id):
    profile = line_bot_api.get_profile(line_user_id)
    line_push, _ = line_sv.get_or_create_line_push(
        user_id=line_user_id,
        display_name=profile.display_name,
    )
    line_sv.switch_is_blocked_by_user_id(user_id=line_user_id, switch=False)
    line_sv.create_line_message(
        push=line_push,
        text=bot_script_dict['follow'],
        msg_attr=MessageAttribute.Bot
    )
    line_bot_api.push_message(
        line_push.user_id,
        messages=TextSendMessage(text=bot_script_dict['follow'])
    )


def unfollow(line_user_id):
    line_sv.switch_is_blocked_by_user_id(user_id=line_user_id, switch=True)
