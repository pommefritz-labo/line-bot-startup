# -*- coding: utf-8 -*-

flex_json_dict = {}
flex_json_dict['default'] = {
    "type": "bubble",
    "styles": {
        "header": {
            "backgroundColor": "#f5fffa"
        },
        "body": {
            "backgroundColor": "#f5fffa"
        }
    },
    "header": {
        "type": "box",
        "layout": "vertical",
        "contents": [
            {
                "type": "text",
                "align": "center",
                "text": "質問"
            },
            {
                "type": "text",
                "wrap": True,
                "text": "ここに質問を記入します。\n質問内容に適した回答を選択して下さい。"
            }
        ]
    },
    "body": {
        "type": "box",
        "layout": "vertical",
        "contents": [
            {
                "type": "button",
                "action": {
                    "type": "postback",
                    "label": "思う",
                    "data": "action=buy&itemid=111"
                },
                "style": "link",
                "color": "#121212"
            },
            {
                "type": "button",
                "action": {
                    "type": "postback",
                    "label": "やや思う",
                    "data": "action=buy&itemid=222"
                },
                "style": "link",
                "color": "#121212"
            },
            {
                "type": "button",
                "action": {
                    "type": "postback",
                    "label": "普通",
                    "data": "action=buy&itemid=333"
                },
                "style": "link",
                "color": "#121212"
            },
            {
                "type": "button",
                "action": {
                    "type": "postback",
                    "label": "やや思わない",
                    "data": "action=buy&itemid=444"
                },
                "style": "link",
                "color": "#121212"
            },
            {
                "type": "button",
                "action": {
                    "type": "postback",
                    "label": "思わない",
                    "data": "action=buy&itemid=555"
                },
                "style": "link",
                "color": "#121212"
            }
        ],
        "paddingAll": "0px"
    }
}
