# -*- coding: utf-8 -*-
import os

from app.modules.line import services as line_sv
from app.modules.line.constants import MessageAttribute
from .bot_script import bot_script_dict
from .flex_templates import flex_json_dict

import linebot
from linebot.models import (
    TextSendMessage,
    TemplateSendMessage,
    ButtonsTemplate,
    PostbackAction,
    FlexSendMessage
)

line_bot_api = linebot.LineBotApi(os.environ['LINE_BOT_API'])


def send_default_text_message(line_user_id, receive_text):
    # receive
    profile = line_bot_api.get_profile(line_user_id)
    line_push, _ = line_sv.get_or_create_line_push(
        user_id=line_user_id,
        display_name=profile.display_name
    )
    line_sv.create_line_message(
        push=line_push,
        text=receive_text,
        msg_attr=MessageAttribute.FromPush,
    )
    # send
    line_sv.create_line_message(
        push=line_push,
        text=bot_script_dict['return'],
        msg_attr=MessageAttribute.Bot
    )
    line_bot_api.push_message(
        line_push.user_id,
        messages=TextSendMessage(text=bot_script_dict['return'])
    )


def send_default_button_message(line_user_id, receive_text):
    profile = line_bot_api.get_profile(line_user_id)
    line_push, _ = line_sv.get_or_create_line_push(
        user_id=line_user_id,
        display_name=profile.display_name
    )
    line_sv.create_line_message(
        push=line_push,
        text=receive_text,
        msg_attr=MessageAttribute.FromPush,
    )
    buttons_template_message = TemplateSendMessage(
        alt_text='Buttons template',
        template=ButtonsTemplate(
            title='質問1',
            text='ここに質問を入力する',
            actions=[
                PostbackAction(
                    label='そう思う',
                    display_text='思う',
                    data='itemid=1'
                ),
                PostbackAction(
                    label='普通',
                    display_text='普通',
                    data='itemid=2'
                ),
                PostbackAction(
                    label='そう思わない',
                    display_text='思わない',
                    data='itemid=3'
                ),
                PostbackAction(
                    label='思わない',
                    display_text='思わない',
                    data='itemid=4'
                )
            ]
        )
    )
    line_bot_api.push_message(
        line_push.user_id,
        messages=buttons_template_message
    )


def send_default_flex_message(line_user_id, receive_text):
    profile = line_bot_api.get_profile(line_user_id)
    line_push, _ = line_sv.get_or_create_line_push(
        user_id=line_user_id,
        display_name=profile.display_name
    )
    line_sv.create_line_message(
        push=line_push,
        text=receive_text,
        msg_attr=MessageAttribute.FromPush,
    )
    flex_message = FlexSendMessage(
        alt_text='flex message template',
        contents=flex_json_dict['default']
    )
    line_bot_api.push_message(
        line_push.user_id,
        messages=flex_message
    )
