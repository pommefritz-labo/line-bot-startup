# -*- coding: utf-8 -*-
import json

from braces.views import CsrfExemptMixin
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views import View

from .event_utils import follow, unfollow
from .event_message import (
    send_default_text_message,
    send_default_button_message,
    send_default_flex_message
)


class CallbackView(CsrfExemptMixin, View):

    @csrf_exempt
    def post(self, request):
        request_json = json.loads(request.body.decode('utf-8'))
        line_user_id = request_json['events'][0]['source']['userId']

        # チャネル設定のWeb hook接続確認
        if line_user_id == 'Udeadbeefdeadbeefdeadbeefdeadbeef':
            pass

        # アカウントがフォローされたかブロック解除されたとき
        elif request_json['events'][0]['type'] == 'follow':
            follow(line_user_id)

        # アカウントがブロックされたとき
        elif request_json['events'][0]['type'] == 'unfollow':
            unfollow(line_user_id)

        # メッセージ受信時
        elif request_json['events'][0]['type'] == 'message':
            receive_text = request_json['events'][0]['message']['text']
            if receive_text == 'フレックス':
                send_default_flex_message(line_user_id, receive_text)
            elif receive_text == 'ボタン':
                send_default_button_message(line_user_id, receive_text)
            else:
                send_default_text_message(line_user_id, receive_text)

        # Postback
        elif request_json['events'][0]['type'] == 'postback':
            postback_data = request_json['events'][0]['postback']['data']
            print(postback_data)

        return HttpResponse()
