# -*- coding: utf-8 -*-
from .views.callback.urls import urlpatterns as callback_urls

app_name = 'callback'

urlpatterns = []
urlpatterns += callback_urls
