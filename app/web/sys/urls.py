# -*- coding: utf-8 -*-
from .views.messages.urls import urlpatterns as messages_urls

app_name = 'sys'

urlpatterns = []
urlpatterns += messages_urls
