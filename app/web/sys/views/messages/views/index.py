import os
from django.shortcuts import redirect
from django.views import View, generic
from django.http import HttpResponse
from django.template import loader

from app.modules.line.models import LinePush
from app.modules.line import services as line_sv

import linebot
from linebot.models import TextSendMessage


line_bot_api = linebot.LineBotApi(os.environ['LINE_BOT_API'])


class UserIndexView(generic.ListView):
    model = LinePush
    template_name = 'messages/pages/user_index.html'


class UserDetailView(View):
    def get(self, request, **kwargs):
        template = loader.get_template('messages/pages/user_detail.html')
        line_push = line_sv.get_by_id(_id=self.kwargs['pk'])
        context = {
            'push': line_push,
            'form': line_sv.get_line_message_form(instance=line_push),
            'message_list': line_sv.filter_by_push(push=line_push),
        }
        return HttpResponse(template.render(context, request))

    def post(self, request, **kwargs):
        template = loader.get_template('messages/pages/user_detail.html')
        line_push = line_sv.get_by_id(_id=self.kwargs['pk'])
        req = request.POST.copy()
        req['push'] = self.kwargs['pk']
        req['msg_attr'] = 3
        form = line_sv.post_line_message_form(req)
        if form.is_valid():
            line_bot_api.push_message(
                line_push.user_id,
                messages=TextSendMessage(text=request.POST['text'])
            )
            form.save()
            return redirect('sys:user_detail', pk=line_push.pk)

        context = {
            'form': form,
        }
        return HttpResponse(template.render(context, request))
