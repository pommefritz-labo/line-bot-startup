# -*- coding: utf-8 -*-
from .index import UserIndexView, UserDetailView

__all__ = [
    # index.py
    'UserIndexView',
    'UserDetailView',
]
