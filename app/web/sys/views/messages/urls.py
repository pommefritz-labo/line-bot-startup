# -*- coding: utf-8 -*-
from django.urls import path

from . import views


urlpatterns = [
    path('', views.UserIndexView.as_view(), name='user_index'),
    path('detail/<int:pk>/', views.UserDetailView.as_view(), name='user_detail'),
]
