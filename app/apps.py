# -*- coding: utf-8 -*-

from django.apps import AppConfig


class CommandsConfig(AppConfig):
    name = 'app'
